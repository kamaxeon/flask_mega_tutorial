## Flask Mega Tutorial

Repositorio para seguir el nuevo [libro](https://learn.miguelgrinberg.com/) de [Miguel Grinberg](https://twitter.com/miguelgrinberg), el proyecto que aborda el libro, es hacer un microblog.

Las razones para hacer un repositorio propio y no leer el [suyo](https://github.com/miguelgrinberg/microblog) son varias:
* Hacerlo poco a poco para enterderlo mejor.
* Intentar usar buenas prácticas y poner el prácticas como pipenv.

## Metolodogía

La idea original es mantener una etiqueta por cada capítulo del libro o hito importante, básicamente es copiar la idea que usa el autor ya que me parece acertada.

## Instalación

### Instalación de python

En el proyecto intentaré usar la última versión de python 3. Para conseguirlo sin tener que depender de la distribución que tenga, usará pyenv. A continuación pongo los pasos que he necesitado para instalarlo en una Ubuntu 14.04.

```
curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash
```

Modificar mi *~/.zshrc* (uso zsh y no bash) acorde a las instrucciones de instalación.

Instalar la nueva versión de python, activándola por defecto para mi usuario

```
pyenv update
pyenv install 3.6.4
pyenv global  3.6.4
pyenv shell 3.6.4
```

Ahora será necesario, lanzar otra terminal para poder usar los cambios.

### Gestión de paquetes

Como este proyecto no está en producción, es perfecto para experimentar. Para la gestión de paquetes se utilizará pipenv, su instalación se hará mediante pip

```
pip install pipenv
```

Para instalar los paquetes de python, sólo hay que hacer lo siguiente:

```
pipenv install
```


## Gestión de migraciones en db

Cada vez que se cambie el modulo referente a SQLAlchemy, hay que hacer la migración y después la actualización en la bbdd.

### Migración
```
FLASK_APP=microblog.py pipenv run flask db migrate -m "Mensaje de migración"
```

### Actualización

```
FLASK_APP=microblog.py pipenv run flask db upgrade
```

## Ejecución

En el directorio principal del proyecto, ejecutar

```
FLASK_APP=microblog.py pipenv run flask run
```
